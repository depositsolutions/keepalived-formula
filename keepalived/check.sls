{% from "keepalived/map.jinja" import keepalived_check with context %}

keepalived-check:
  file.directory:
  - name: {{ keepalived_check.script_dir }}
  - makedirs: True
  - user: root
  - group: root
  - mode: 0775

{% if keepalived_check.chk %}
{% for name, chk in keepalived_check.chk.items() %}
keepalived-check-script-{{ name }}:
  file.managed:
    - name: {{ keepalived_check.script_dir }}/{{ name }}.sh
    - user: root
    - group: root
    - mode: 0755
    - context:
        keepalived_check:
          check_secret: {{ chk.secret }}
    - template: jinja
    - contents: |
      {{ chk.script | indent(8) }}

{% endfor %}
{% endif %}
